package service.exception;

public class CamposVaciosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2307873248247031333L;

	public CamposVaciosException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CamposVaciosException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CamposVaciosException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CamposVaciosException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CamposVaciosException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
