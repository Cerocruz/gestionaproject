package service.mongo;

import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

import properties.Config;

public class MongoUtil {

	private static MongoClient mongoClient;

	public static MongoClient getMongoClient() {
		if (mongoClient == null) {
			initMongoCliente();
		}
		return mongoClient;
	}

	private static void initMongoCliente() {
		ConnectionString connectionString = new ConnectionString(Config.getInstance().getProperty("ConnectionString"));
		MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(connectionString)
				.serverApi(ServerApi.builder().version(ServerApiVersion.V1).build()).build();
		mongoClient = MongoClients.create(settings);
	}

	public static MongoDatabase getDatabase(String dbName) {
		if (mongoClient == null) {
			initMongoCliente();
		}
		MongoDatabase database = mongoClient.getDatabase(dbName);
		CodecRegistry defaultCodecRegistry = MongoClientSettings.getDefaultCodecRegistry();
		CodecProvider codecProvider = PojoCodecProvider.builder().automatic(true).build();
		CodecRegistry pojoCodecRegistry = CodecRegistries.fromProviders(codecProvider);
		CodecRegistry codecRegistry = CodecRegistries.fromRegistries(defaultCodecRegistry, pojoCodecRegistry);
		return database.withCodecRegistry(codecRegistry);

	}
}