package service.mongo;

import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import modelo.Movimiento;
import modelo.Usuario;
import properties.Config;
import service.exception.NotFoundException;

public class UsuarioService {

	public static void main(String[] args) {
		UsuarioService us = new UsuarioService();
		Usuario user = new Usuario();
		user.setUserName("gon");
		try {
			System.out.println(us.consultarUsuario(user));
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

	public UsuarioService() {
	}

	public void crearUsuario(Usuario usuario) {
		MongoCollection<Usuario> collection = obtenerCollection();
		try {
			consultarUsuario(usuario);
		} catch (NotFoundException e) {
			collection.insertOne(usuario);
		}
	}

	public Usuario consultarUsuario(Usuario usuario) throws NotFoundException {
		MongoCollection<Usuario> collection = obtenerCollection();
		Bson filtro = Filters.eq("userName", usuario.getUserName());
		FindIterable<Usuario> userBrowser = collection.find(filtro);
		Usuario user = userBrowser.first();
		if (user == null) {

			throw new NotFoundException("Usuario no registrado en la base de datos.");
		} else {
			return user;
		}
	}

	public void añadirMovimiento(String userName, Movimiento movimiento) {
		MongoCollection<Usuario> collection = obtenerCollection();
		Bson filtro = Filters.eq("userName", userName);
		Bson update = Updates.push("movimientos", movimiento);
		collection.updateOne(filtro, update);
	}

	private MongoCollection<Usuario> obtenerCollection() {
		MongoDatabase db = MongoUtil.getDatabase(Config.getInstance().getProperty("database"));
		MongoCollection<Usuario> collection = db.getCollection(Config.getInstance().getProperty("collection"),
				Usuario.class);
		return collection;
	}

}
