package modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Usuario {

	private String userName;
	private String password;
	private List<Movimiento> movimientos;

	public Usuario() {
		movimientos = new ArrayList<>();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Movimiento> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<Movimiento> movimientos) {
		this.movimientos = movimientos;
	}

	@Override
	public int hashCode() {
		return Objects.hash(userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		return Objects.equals(userName, other.userName);

	}

	@Override
	public String toString() {
		return "Usuario [userName=" + userName + ", password=" + password + ", movimientos=" + movimientos + "]";
	}

}
