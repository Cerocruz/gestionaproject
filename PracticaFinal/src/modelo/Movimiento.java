package modelo;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Movimiento {
	public static final String TIPO_BENIFIT = "Benefit";
	public static final String TIPO_GASTO = "Gasto";

	private Integer id;
	private BigDecimal cantidad;
	private String tipo;
	private String descripcion;
	private LocalDate fecha;

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		if (cantidad.compareTo(BigDecimal.ZERO) < 0) {
			setTipo(TIPO_GASTO);
		} else {
			setTipo(TIPO_BENIFIT);
		}
		this.cantidad = cantidad;
	}

	public String getTipo() {
		return tipo;
	}

	private void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Movimiento [id=" + id + ", cantidad=" + cantidad + ", tipo=" + tipo + ", descripcion=" + descripcion
				+ ", fecha=" + fecha + "]";
	}

}
