package gui;

import gui.controller.AppController;
import gui.controller.GestionaPath;
import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		AppController controller = new AppController(primaryStage);
		controller.cambiarVista(GestionaPath.FXML_INICIO);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch();
	}

}
