package gui;

public class Launcher {

	public static void main(String[] args) {
		System.setProperty("log4j.configurationFile", "config/log.properties");
		App.main(args);
	}

}
