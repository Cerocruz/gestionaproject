package gui.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Movimiento;

public class ConsultarController extends AppController {

	private final static Logger Log = LogManager.getLogger(ConsultarController.class.getName());

	private static final String BENEFITS_LABEL_TEXT = "BENEFIT TOTAL:  ";
	private static final String DEFICITS_LABEL_TEXT = "DEFICIT TOTAL: ";
	private static final String TOTAL_LABEL_TEXT = "TOTAL: ";

	private static final String BENEFIT = "BENEFITS";
	private static final String DEFICIT = "DEFICITS";

	private static final String ENERO = "ENERO";
	private static final String FEBRERO = "FEBRERO";
	private static final String MARZO = "MARZO";
	private static final String ABRIL = "ABRIL";
	private static final String MAYO = "MAYO";
	private static final String JUNIO = "JUNIO";
	private static final String JULIO = "JULIO";
	private static final String AGOSTO = "AGOSTO";
	private static final String SEPTIEMBRE = "SEPTIEMBRE";
	private static final String OCTUBRE = "OCTUBRE";
	private static final String NOVIEMBRE = "NOVIEMBRE";
	private static final String DICIEMBRE = "DICIEMBRE";
	private static final String TODOS = "Todos";

	@FXML
	private TableView<Movimiento> tabla;
	private ObservableList<Movimiento> datos;
	@FXML
	private TableColumn<Movimiento, Integer> cId;
	@FXML
	private TableColumn<Movimiento, BigDecimal> cCantidad;
	@FXML
	private TableColumn<Movimiento, String> cDescripcion;
	@FXML
	private TableColumn<Movimiento, String> cTipo;
	@FXML
	private TableColumn<Movimiento, LocalDate> cFecha;
	@FXML
	private Label benefitLabel;
	private BigDecimal benefits;
	@FXML
	private Label deficitLabel;
	private BigDecimal deficits;
	@FXML
	private Label totalLabel;
	private BigDecimal total;
	@FXML
	private ComboBox<String> tipoCombo;
	@FXML
	private ComboBox<String> mesCombo;

	@FXML
	public void initialize() {
		rellenarComboBox();

		PropertyValueFactory<Movimiento, Integer> factoryIdValue = new PropertyValueFactory<>("id");
		PropertyValueFactory<Movimiento, BigDecimal> factoryCantidadValue = new PropertyValueFactory<>("cantidad");
		PropertyValueFactory<Movimiento, String> factoryDescripcionValue = new PropertyValueFactory<>("descripcion");
		PropertyValueFactory<Movimiento, String> factoryTipoValue = new PropertyValueFactory<>("tipo");
		PropertyValueFactory<Movimiento, LocalDate> factoryFechaValue = new PropertyValueFactory<>("fecha");

		cFecha.setCellFactory(column -> {
			TableCell<Movimiento, LocalDate> cell = new TableCell<Movimiento, LocalDate>() {
				DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

				@Override
				protected void updateItem(LocalDate item, boolean empty) {
					super.updateItem(item, empty);
					if (empty) {
						setText(null);
					} else {
						setText(dateFormatter.format(item));
					}
				}
			};

			return cell;
		});

		cId.setCellValueFactory(factoryIdValue);
		cCantidad.setCellValueFactory(factoryCantidadValue);
		cDescripcion.setCellValueFactory(factoryDescripcionValue);
		cTipo.setCellValueFactory(factoryTipoValue);
		cFecha.setCellValueFactory(factoryFechaValue);

		datos = FXCollections.observableArrayList();
		datos.addAll(getUsuario().getMovimientos());
		tabla.setItems(datos);

		calcularCantidades();
	}

	private void calcularCantidades() {
		benefits = BigDecimal.ZERO;
		deficits = BigDecimal.ZERO;
		total = BigDecimal.ZERO;
		for (int i = 0; i < datos.size(); i++) {
			if (datos.get(i).getTipo().equals(Movimiento.TIPO_BENIFIT)) {
				benefits = benefits.add(datos.get(i).getCantidad());
			} else {
				deficits = deficits.add(datos.get(i).getCantidad());
			}
		}
		total = benefits.add(deficits);

		benefitLabel.setText(BENEFITS_LABEL_TEXT + benefits);
		deficitLabel.setText(DEFICITS_LABEL_TEXT + deficits);
		totalLabel.setText(TOTAL_LABEL_TEXT + total);
	}

	public void clickVolver() {
		cambiarVista(GestionaPath.FXML_MENU_INICIO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Menu Inicio desde pantalla Consultar.");
	}

	public void filtrar() {
		datos.clear();
		datos.addAll(getUsuario().getMovimientos());
		if (!tipoCombo.getValue().equals(TODOS)) {
			String tipo = "";
			if (tipoCombo.getValue().equals(BENEFIT)) {
				tipo = Movimiento.TIPO_BENIFIT;
			} else {
				tipo = Movimiento.TIPO_GASTO;
			}
			Iterator<Movimiento> iterator = datos.iterator();
			while (iterator.hasNext()) {
				if (!iterator.next().getTipo().equals(tipo)) {
					iterator.remove();
				}
			}
		}
		if (!mesCombo.getValue().equals(TODOS)) {
			Month mes = null;
			if (mesCombo.getValue().equals(ENERO)) {
				mes = Month.JANUARY;
			} else if (mesCombo.getValue().equals(FEBRERO)) {
				mes = Month.FEBRUARY;
			} else if (mesCombo.getValue().equals(MARZO)) {
				mes = Month.MARCH;
			} else if (mesCombo.getValue().equals(ABRIL)) {
				mes = Month.APRIL;
			} else if (mesCombo.getValue().equals(MAYO)) {
				mes = Month.MAY;
			} else if (mesCombo.getValue().equals(JUNIO)) {
				mes = Month.JUNE;
			} else if (mesCombo.getValue().equals(JULIO)) {
				mes = Month.JULY;
			} else if (mesCombo.getValue().equals(AGOSTO)) {
				mes = Month.AUGUST;
			} else if (mesCombo.getValue().equals(SEPTIEMBRE)) {
				mes = Month.SEPTEMBER;
			} else if (mesCombo.getValue().equals(OCTUBRE)) {
				mes = Month.OCTOBER;
			} else if (mesCombo.getValue().equals(NOVIEMBRE)) {
				mes = Month.NOVEMBER;
			} else {
				mes = Month.DECEMBER;
			}
			Iterator<Movimiento> iterator = datos.iterator();
			while (iterator.hasNext()) {
				if (!iterator.next().getFecha().getMonth().equals(mes)) {
					iterator.remove();
				}
			}
		}
		calcularCantidades();
	}

	private void rellenarComboBox() {
		ObservableList<String> items = FXCollections.observableArrayList();
		items.addAll(TODOS, ENERO, FEBRERO, MARZO, ABRIL, MAYO, JUNIO, JULIO, AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE,
				DICIEMBRE);
		mesCombo.setItems(items);

		ObservableList<String> items2 = FXCollections.observableArrayList();
		items2.addAll(TODOS, BENEFIT, DEFICIT);
		tipoCombo.setItems(items2);

		tipoCombo.setValue(TODOS);
		mesCombo.setValue(TODOS);
	}

}
