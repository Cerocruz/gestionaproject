package gui.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import modelo.Movimiento;
import javafx.scene.control.Alert.AlertType;
import service.exception.CamposVaciosException;
import service.exception.NotFoundException;

public class EliminarMovimientoController extends AppController {

	@FXML
	private TextField idField;
	private final static Logger Log = LogManager.getLogger(EliminarMovimientoController.class.getName());

	@FXML
	public void clickEliminar() {
		try {
			checkFields();
			Integer id = Integer.parseInt(idField.getText());
			Movimiento mov = null;
			List<Movimiento> lista = getUsuario().getMovimientos();
			for (int i = 0; i < lista.size(); i++) {
				if (lista.get(i).getId() == id) {
					mov = lista.get(i);
					break;
				}
			}
			if (mov == null) {
				Alert a = new Alert(AlertType.WARNING);
				a.setHeaderText("ERROR");
				a.setContentText("El id del movimiento no corresponde a ningún movimiento existente");
				a.showAndWait();
				Log.log(Level.ERROR, "El id del movimiento no corresponde a ningún movimiento existente");
				return;
			}
			mov.setCantidad(mov.getCantidad().multiply(new BigDecimal(-1)));
			mov.setDescripcion("Anulador del mov. de ID: " + mov.getId());
			mov.setId(lista.size() + 1);
			mov.setFecha(LocalDate.now());
			getUsuarioService().añadirMovimiento(getUsuario().getUserName(), mov);
			setUsuario(getUsuarioService().consultarUsuario(getUsuario()));
			Alert a = new Alert(AlertType.INFORMATION);
			a.setHeaderText("MOVIMIENTO ANULADO");
			a.setContentText("El movimiento anulador ha sido registrado satisfactoriamente.");
			a.showAndWait();
			Log.log(Level.DEBUG, "Se ha borrado con exito el movimiento de ID: " + mov.getId());
		} catch (CamposVaciosException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR");
			a.setContentText(e.getMessage());
			a.showAndWait();
			Log.log(Level.ERROR, e);
		} catch (NumberFormatException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR");
			a.setContentText(e.getMessage());
			a.showAndWait();
			Log.log(Level.ERROR, e);
		} catch (NotFoundException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR INESPERADO");
			a.setContentText(e.getMessage());
			a.showAndWait();
			Log.log(Level.ERROR, e);
		} finally {
			idField.clear();
		}

	}

	public void clickCancelar() {
		cambiarVista(GestionaPath.FXML_MENU_INICIO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Menu Inicio desde pantalla eliminar.");
	}

	public void checkFields() throws CamposVaciosException {
		if (idField.getText() == null) {
			throw new CamposVaciosException("Campo id vacío.");
		}
	}

}
