package gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import modelo.Usuario;
import service.exception.CamposVaciosException;
import service.exception.NotFoundException;

public class RegistrarController extends AppController {

	private final static Logger Log = LogManager.getLogger(InicioController.class.getName());

	@FXML
	private TextField registrarField;
	@FXML
	private TextField passwordField;

	public void clickRegistrar() {
		try {
			checkCamposVacios();
			verificarUser();
		} catch (CamposVaciosException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR");
			a.setContentText(e.getMessage());
			Log.log(Level.ERROR, e);
			a.showAndWait();
		} catch (NotFoundException e) {
			Usuario usuario = new Usuario();
			usuario.setUserName(registrarField.getText());
			usuario.setPassword(passwordField.getText());
			getUsuarioService().crearUsuario(usuario);
			Alert a = new Alert(AlertType.INFORMATION);
			a.setHeaderText("USUARIO CREADO");
			a.setContentText("Nuevo usuario registrado. Bienvenido :).");
			Log.log(Level.DEBUG, "Nuevo usuario creado y registrado satisfactoriamente.");
			a.showAndWait();
			clickVolver();
		}
	}

	public void clickVolver() {
		cambiarVista(GestionaPath.FXML_INICIO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Inicio desde pantalla Registrar.");
	}

	public void checkCamposVacios() throws CamposVaciosException {
		Boolean campoVacio = false;
		String msgError = "";
		if (registrarField.getText().isBlank()) {
			msgError = msgError + "El campo 'Username' debe rellenarse.\n";
			campoVacio = true;
		}
		if (passwordField.getText().isBlank()) {
			msgError = msgError + "El campo 'Password' debe rellenarse.";
			campoVacio = true;
		}
		if (campoVacio) {
			throw new CamposVaciosException(msgError);
		}
	}

	private void verificarUser() throws NotFoundException {
		Usuario usuario = new Usuario();
		usuario.setUserName(registrarField.getText());
		getUsuarioService().consultarUsuario(usuario);

	}
}
