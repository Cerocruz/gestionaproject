package gui.controller;

import java.io.IOException;
import java.net.URL;

import gui.App;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import modelo.Usuario;
import service.mongo.UsuarioService;

public class AppController {

	private static Stage primaryStage;
	private static UsuarioService usuarioService;
	private static Usuario usuario;

	public AppController() {
		usuarioService = new UsuarioService();
	}

	public AppController(Stage stage) {
		primaryStage = stage;
	}

	public AppController cambiarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			Scene scene;
			scene = new Scene(loader.load(), 600, 400);
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			return loader.getController();
		} catch (IOException e) {
			throw new RuntimeException("Fallo al cargar ruta:" + fxml, e);
		}
	}

	public Parent cargarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			return loader.load();
		} catch (IOException e) {
			throw new RuntimeException("Fallo al cargar ruta:" + fxml, e);
		}
	}

	public static UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public static void setUsuarioService(UsuarioService usuarioService) {
		AppController.usuarioService = usuarioService;
	}

	public static Usuario getUsuario() {
		return usuario;
	}

	public static void setUsuario(Usuario usuario) {
		AppController.usuario = usuario;
	}

}
