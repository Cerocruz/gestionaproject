package gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;

public class MenuInicioController extends AppController {

	private final static Logger Log = LogManager.getLogger(MenuInicioController.class.getName());

	@FXML
	public void clickConsultarMovimientos() {
		cambiarVista(GestionaPath.FXML_CONSULTAR);
		Log.log(Level.DEBUG, "Cambiando a pantalla Consultar desde pantalla Menu Inicio");
	}

	@FXML
	public void clickAñadirMovimientos() {
		cambiarVista(GestionaPath.FXML_CREAR_MOVIMIENTO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Añadir Movimientos desde pantalla Menu Inicio");
	}

	@FXML
	public void clickEliminarMovimiento() {
		cambiarVista(GestionaPath.FXML_ELIMINAR_MOVIMIENTO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Eliminar Movimientos desde pantalla Menu Inicio");
	}

	@FXML
	public void clickSalir() {
		System.exit(0);
	}

	@FXML
	public void clickCerrarSesionImage() {
		cambiarVista(GestionaPath.FXML_INICIO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Inicio desde pantalla Menu Inicio");
		Log.log(Level.DEBUG, "Delogueado usuario");
	}

}
