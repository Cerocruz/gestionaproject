package gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import modelo.Usuario;
import service.exception.CamposVaciosException;
import service.exception.NotFoundException;

public class InicioController extends AppController {

	private final static Logger Log = LogManager.getLogger(InicioController.class.getName());

	@FXML
	private TextField usuarioField;

	@FXML
	private PasswordField passwordFieldNonVisible;

	@FXML
	private TextField fieldVisible;

	@FXML
	private CheckBox visibleMode;

	@FXML
	public void clickEntrar() {
		try {
			checkCamposVacios();
			Usuario user = verificarUser();
			setUsuario(user);
			cambiarVista(GestionaPath.FXML_MENU_INICIO);
			Log.log(Level.DEBUG, "Se ha iniciado sesión satisfactoriamente.");
		} catch (CamposVaciosException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR");
			a.setContentText(e.getMessage());
			Log.log(Level.ERROR, e);
			a.showAndWait();
		} catch (NotFoundException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR");
			a.setContentText(e.getMessage());
			Log.log(Level.ERROR, e);
			a.showAndWait();
		}

	}
	
	public void clickRegistrar() {
		cambiarVista(GestionaPath.FXML_REGISTRAR);
		Log.log(Level.DEBUG, "Cambiando a pantalla Registrar desde pantalla Inicio.");
	}

	private void checkCamposVacios() throws CamposVaciosException {
		Boolean campoVacio = false;
		String msgError = "";
		if (usuarioField.getText().isBlank()) {
			msgError = msgError + "El campo 'Username' debe rellenarse.\n";
			campoVacio = true;
		}
		if (visibleMode.isSelected()) {
			if (passwordFieldNonVisible.getText().isBlank()) {
				msgError = msgError + "El campo 'Contraseña' debe rellenarse.";
				campoVacio = true;
			}
		} else {
			if (fieldVisible.getText().isBlank()) {
				msgError = msgError + "El campo 'Contraseña' debe rellenarse.";
				campoVacio = true;
			}
		}
		if (campoVacio) {
			throw new CamposVaciosException(msgError);
		}
	}

	private Usuario verificarUser() throws NotFoundException {
		Usuario usuario = new Usuario();
		usuario.setUserName(usuarioField.getText());
		usuario = getUsuarioService().consultarUsuario(usuario);
		if (visibleMode.isSelected()) {
			if (!usuario.getPassword().equals(passwordFieldNonVisible.getText())) {
				throw new NotFoundException("Contraseña incorrecta.");
			}
		} else {
			if (!usuario.getPassword().equals(fieldVisible.getText())) {
				throw new NotFoundException("Contraseña incorrecta.");
			}
		}
		return usuario;
	}

	@FXML
	public void clickSalir() {
		System.exit(0);
	}

	@FXML
	public void cambiarPasswordChckbx() {
		if (visibleMode.isSelected()) {
			String clave = fieldVisible.getText();
			ocultarFieldVisible(clave);
			mostrarPasswordField();
			Log.log(Level.DEBUG, "Password no visible");
		} else {
			String clave = passwordFieldNonVisible.getText();
			ocultarPasswordField();
			mostrarFieldVisible(clave);
			Log.log(Level.DEBUG, "Password visible");
		}
	}

	private void mostrarPasswordField() {
		passwordFieldNonVisible.setOpacity(1);
		passwordFieldNonVisible.setEditable(true);
		passwordFieldNonVisible.requestFocus();
	}

	private void ocultarPasswordField() {
		passwordFieldNonVisible.setOpacity(0);
		passwordFieldNonVisible.setEditable(false);
		passwordFieldNonVisible.toBack();
	}

	private void ocultarFieldVisible(String clave) {
		fieldVisible.setText(clave);
		fieldVisible.setOpacity(0);
		fieldVisible.setEditable(false);
		fieldVisible.toBack();
	}

	private void mostrarFieldVisible(String clave) {
		fieldVisible.setText(clave);
		fieldVisible.setOpacity(1);
		fieldVisible.setEditable(true);
		fieldVisible.requestFocus();
	}

}
