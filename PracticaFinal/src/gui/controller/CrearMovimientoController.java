package gui.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.util.converter.LocalDateStringConverter;
import modelo.Movimiento;
import service.exception.CamposVaciosException;
import service.exception.NotFoundException;

public class CrearMovimientoController extends AppController {

	private final static Logger Log = LogManager.getLogger(CrearMovimientoController.class.getName());

	@FXML
	private TextField cantidadField;

	@FXML
	private DatePicker fechaDatePicker;

	@FXML
	private TextArea descripcionArea;

	@FXML
	public void initialize() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
		fechaDatePicker.setValue(LocalDate.now());
		fechaDatePicker.setConverter(new LocalDateStringConverter(formatter, null));
	}

	@FXML
	public void clickAgregar() {
		try {
			checkCamposVacios();
			BigDecimal cantidad = new BigDecimal(cantidadField.getText());
			LocalDate fecha = fechaDatePicker.getValue();
			String descripcion = descripcionArea.getText();
			Movimiento movimiento = new Movimiento();
			movimiento.setId(getUsuario().getMovimientos().size() + 1);
			movimiento.setCantidad(cantidad.setScale(2, RoundingMode.HALF_UP));
			movimiento.setFecha(fecha);
			movimiento.setDescripcion(descripcion);
			getUsuarioService().añadirMovimiento(getUsuario().getUserName(), movimiento);
			setUsuario(getUsuarioService().consultarUsuario(getUsuario()));
			Alert a = new Alert(AlertType.INFORMATION);
			a.setHeaderText("MOVIMIENTO AGREGADO");
			a.setContentText("El movimiento ha sido registrado satisfactoriamente.");
			a.showAndWait();
			Log.log(Level.DEBUG, "Se ha creado con exito el nuevo movimiento de ID: " + movimiento.getId());
		} catch (NumberFormatException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("CANTIDAD INADECUADA");
			a.setContentText("La cantidad debe ser numérica.\n" + e.getMessage());
			a.showAndWait();
			Log.log(Level.ERROR, e);
		} catch (CamposVaciosException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR");
			a.setContentText(e.getMessage());
			a.showAndWait();
			Log.log(Level.ERROR, e);
		} catch (NotFoundException e) {
			Alert a = new Alert(AlertType.WARNING);
			a.setHeaderText("ERROR INESPERADO");
			a.setContentText(e.getMessage());
			a.showAndWait();
			Log.log(Level.ERROR, e);
		} finally {
			vaciarCampos();
		}
	}

	private void checkCamposVacios() throws CamposVaciosException, NumberFormatException {
		if (cantidadField.getText().isBlank()) {
			throw new CamposVaciosException("El campo 'Cantidad' debe rellenarse");
		}
		if (fechaDatePicker.getValue() == null) {
			throw new CamposVaciosException("El campo 'Fecha' debe rellenarse");
		}
	}

	private void vaciarCampos() {
		cantidadField.clear();
		fechaDatePicker.setValue(LocalDate.now());
		descripcionArea.clear();
	}

	@FXML
	public void clickCancelar() {
		cambiarVista(GestionaPath.FXML_MENU_INICIO);
		Log.log(Level.DEBUG, "Cambiando a pantalla Menu Inicio desde pantalla Crear movimientos.");
	}
}
