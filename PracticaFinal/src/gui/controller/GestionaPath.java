package gui.controller;

public class GestionaPath {

	public static final String FXML_INICIO = "/javafx/gui/fxml/inicio.fxml";
	public static final String FXML_REGISTRAR = "/javafx/gui/fxml/registrar.fxml";
	public static final String FXML_MENU_INICIO = "/javafx/gui/fxml/menuInicio.fxml";
	public static final String FXML_CREAR_MOVIMIENTO = "/javafx/gui/fxml/crearMovimiento.fxml";
	public static final String FXML_ELIMINAR_MOVIMIENTO = "/javafx/gui/fxml/eliminarMovimiento.fxml";
	public static final String FXML_CONSULTAR = "/javafx/gui/fxml/consultar.fxml";

}
