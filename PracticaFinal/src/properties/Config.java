package properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private static Config INSTANCE = null;

	private static final String CONFIGPATH = "/config/config.conf";

	private Properties properties;

	private Config() {
		properties = new Properties();
		InputStream is = getClass().getResourceAsStream(CONFIGPATH);
		try {
			properties.load(is);
		} catch (IOException e) {
			System.err.println("No se pudo cargar el fichero de configuración");
			e.printStackTrace();
		}
	}

	public static Config getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Config();
		}
		return INSTANCE;
	}

	public String getProperty(String name) {
		return properties.getProperty(name);
	}

	public Boolean getPropertyAsBoolean(String name) {
		return Boolean.parseBoolean(getProperty(name));
	}

	public Integer getPropertyAsInteger(String name) {
		return Integer.parseInt(getProperty(name));
	}

}
